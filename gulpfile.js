var gulp          = require('gulp'),
    less          = require('gulp-less'),
    autoprefixer  = require('gulp-autoprefixer'),
    minifyCSS     = require('gulp-minify-css'),
    jshint        = require('gulp-jshint'),
    uglify        = require('gulp-uglify'),
    rename        = require('gulp-rename'),
    concat        = require('gulp-concat'),
    concatCSS     = require('gulp-concat-css'),
    notify        = require('gulp-notify'),
    cache         = require('gulp-cache'),
    del           = require('del')
    stripComments = require('gulp-strip-css-comments')

    input  = {
        css: 'src/css/*.less',
        js: 'src/js/**/*.js'
    }

    output = {
        css: 'assets/css/',
        js: 'assets/js/'
    };

gulp.task('styles', function() {
    return gulp.src(input.css)
        .pipe(less())
        .pipe(autoprefixer('last 2 version'))
        .pipe(concatCSS('main.css'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(minifyCSS({
            compatibility: 'ie9'
        }))
        .pipe(stripComments({
            preserve: false
        }))
        .pipe(gulp.dest(output.css))
        .pipe(notify({
            message: 'Styles task complete'
        }));
});

gulp.task('scripts', function() {
    return gulp.src(input.js)
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('default'))
        .pipe(concat('main.js'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(gulp.dest(output.js))
        .pipe(notify({
            message: 'Scripts task complete' }
        ));
});

gulp.task('clean', function(callback) {
    del([output.css, output.js], callback)
});

gulp.task('watch', function() {
    gulp.watch(input.css, ['styles']);
    gulp.watch(input.js, ['scripts']);
});

gulp.task('default', function() {
    gulp.start(['clean', 'styles', 'scripts']);
});